
import { addProduct, editProduct } from '@/api/product'  // 导入商品信息相关接口

const product = {
    state: {
        addProductDialog: {
            title: '新增商品信息',
            type: 'add'
        },
        productSearch: {
            id: '',
            sname: ''
        },
        deleteAllProductBtnDisabled: true
    },
    getters: {
        addProductDialog: state => state.addProductDialog,
        productSearchData: state => state.productSearch,
        deleteAllProductBtnDisabled: state => state.deleteAllProductBtnDisabled,
    },
    mutations: {
        SET_PRODUCTDIALOG_TITLE: (state, type) => {
            if (type === 'add') {
                state.addProductDialog.title = '新增商品信息'
                state.addProductDialog.type = 'add'
            } else {
                state.addProductDialog.title = '编辑商品信息'
                state.addProductDialog.type = 'edit'
            }
        },
        PRODUCTD_SEARCH: (state, payload) => {
            state.productSearch = payload;
        },
        SET_PRODUCT_SEARCHBTN_DISABLED: (state, payload) => {
            state.deleteAllProductBtnDisabled = payload;
        }
    },
    actions: {
        // 获取商品列表
        GetMoneyIncomePay({ commit }, reqData) {
            // return new Promise(resolve => {
            //     getMoneyIncomePay(reqData).then(response => {
            //         const data = response.data
            //         resolve(data)
            //     })
            // })
        }


    }
}

export default product
