const zh = {
  // layout
  commons: {
    xiaoai: 'SemiAuto',
    admin: '管理员',
    editor: '赵晓编',
    quit: '退出',
    hi: '您好',
    index: '首页',
    userManage: '用户管理',
    share: '分享功能',
    infoManage: '信息管理',
    infoShow: '商品信息',
    infoShow1: '商品信息子菜单1',
    infoShow2: '商品信息子菜单2',
    infoShow3: '商品信息子菜单3',
    infoShow4: '商品信息子菜单4',
    infoShow5: '商品信息子菜单5',
    infoModify: '修改信息',
    infoModify1: '修改信息子菜单1',
    infoModify2: '修改信息子菜单2',
    infoModify3: '修改信息子菜单3',
    fundManage: '资金管理',
    fundList: '资金流水',
    chinaTabsList: '区域投资',
    fundData: '资金数据',
    fundPosition: '投资分布',
    typePosition: '项目分布',
    incomePayPosition: '收支分布',
    permission: '权限设置',
    pagePer: '页面权限',
    directivePer: '按钮权限',
    errorPage: '错误页面',
    page401: '401',
    page404: '404',
    wechatNumber: '南方测绘'
  },
  index: {
    yearLoss: '年度总收益',
    yearProfit: '年度收益率',
    potentialInvestor: '潜在用户',
    intentionInvestor: '意向用户',
    waitExamineInvestor: '待审用户',
    examiningInvestor: '审核中用户',
    tenMillion: '千万元',
    person: '人'
  }
}

export default zh;