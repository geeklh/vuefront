//商品模块api
import request from '@/utils/axios'

// 请求商品列表
export function getProductList(reqData) {
    return request({
        url: '/product/findAllPro.do',
        method: 'get',
        data: reqData
    })
}
// 查询商品
export function searchProduct(reqData) {
    return request({
        url: '/product/findbyname.do',
        method: 'get',
        params: reqData
    })
}
// 添加商品
export function addProduct(reqData) {
    console.log("reqData", reqData);
    return request({
        url: '/product/addpro.do',
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: reqData
    })
}
// 删除商品
export function deleteProduct(reqData) {
    return request({
        url: '/product/deletepro.do',
        method: 'delete',
        params: reqData
    })
}
// 批量删除商品
export function deleteAllProduct(reqData) {
    return request({
        url: '/product/deleteProvider.do',
        method: 'post',
        data: reqData,
    })
}
// 编辑商品
export function updateProduct(reqData) {
    return request({
        url: '/product/updatepro.do',
        method: 'put',
        data: reqData

    })
}