import request from '@/utils/axios'
import qs from "qs";


export function login(params) {
  return request({
    url: '/user/login',
    method: 'get',
    data: params
  })
}
export function logout(params) {
  return request({
    url: '/user/logout',
    method: 'get',
    data: params
  })
}


export function getUserInfo(params) {
  return request({
    url: '/user/info/get',
    method: 'get',
    data: params
  })
}

// export function getUserList(reqData) {
//   return request({
//     url: '/user/list/get',
//     method: 'get',
//     data: reqData
//   })
// }
// 请求用户列表
export function getUserList(reqData) {
  return request({
    url: '/admin/findAllUser.do',
    method: 'get',
    // data: reqData
  })
}
// 条件查询用户列表
export function searchUserList(reqData) {
  return request({
    url: '/admin/findUserByNameOrPhone.do',
    method: 'get',
    params: reqData
  })
}
// 删除用户
export function deleteUser(reqData) {
  return request({
    url: '/admin/deleteUser.do',
    method: 'delete',
    data: reqData
  })
}
